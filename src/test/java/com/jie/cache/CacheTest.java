package com.jie.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.SimpleCrmApplication;
import com.alibaba.fastjson.JSONObject;
import com.jie.cache.support.CacheDef;
import com.jie.crm.common.R;
import com.jie.crm.domain.CustomerLoanType;
import com.jie.crm.enumeration.ECustomerState;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleCrmApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class CacheTest {

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Test
	@Ignore
	public void get() throws Exception {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("LIKE_customerName", "李四");
		MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
		multiValueMap.add("dataMap", JSONObject.toJSONString(paramMap));
		multiValueMap.add("page", "1");
		multiValueMap.add("rows", "10");
		R result = testRestTemplate.postForObject("/customer/query", multiValueMap, R.class);
		Assert.assertEquals(result.getCode(), "ok");
	}

	@Test
	public void intput() throws Exception {
		String result = testRestTemplate.getForObject("/customer/input", String.class);
		System.out.println(result);
	}

	@Test
	@Ignore
	public void cache() {
		List<CacheDef> list = Caches.getList(ECustomerState.class);
		System.out.println(list);
		Assert.assertNotNull(Caches.getList(CustomerLoanType.class));
	}
}
