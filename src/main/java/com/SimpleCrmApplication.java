package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableCaching
@ComponentScan(basePackages = { "com.jie" })
@EnableScheduling
@EnableAutoConfiguration
@EnableConfigurationProperties
public class SimpleCrmApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleCrmApplication.class, args);
    }
}
