package com.jie.cache.provider;

import java.beans.Introspector;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import net.oschina.j2cache.CacheException;
import net.oschina.j2cache.CacheObject;

import org.springframework.data.jpa.repository.CommonJpaRepository;
import org.springframework.util.ClassUtils;

import com.jie.cache.support.CacheDef;

import spring.data.jpa.tools.DBHelper;
import spring.data.jpa.tools.Reflections;
import spring.data.jpa.tools.SpringContextHolder;

public class DBCacheProvider extends CacheProvider {

	public DBCacheProvider(CacheDef def) {
		super(def);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getList() {
		Class<?> clazz = def.getClassName();
		Entity entity = clazz.getDeclaredAnnotation(Entity.class);
		if (null == entity) {
			throw new CacheException(def.getClassName() + "不是实体类");
		}
		CacheObject cache = j2cache.get("default", clazz.getName());
		if (cache.getValue() != null) {
			return (List<T>) cache.getValue();
		}
		List<CacheDef> list = new ArrayList<>();
		String repoName = Introspector.decapitalize(ClassUtils.getShortName(clazz) + "Repository");
		CommonJpaRepository<?, ?> repository = SpringContextHolder.getBean(repoName);
		List<?> objects = DBHelper.bind(repository).findAll();
		for (Object obj : objects) {
			CacheDef cacheDef = new CacheDef();
			cacheDef.setNameField(String.valueOf(Reflections.getFieldValue(obj, def.getNameField())));
			cacheDef.setValueField(String.valueOf(Reflections.getFieldValue(obj, def.getValueField())));
			list.add(cacheDef);
		}
		j2cache.set("default", clazz.getName(), list);
		return (List<T>) list;
	}

}
