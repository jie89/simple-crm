package com.jie.cache.provider;

import java.util.ArrayList;
import java.util.List;

import com.jie.cache.support.CacheDef;

import net.oschina.j2cache.CacheException;
import net.oschina.j2cache.CacheObject;
import spring.data.jpa.tools.Reflections;

public class EnumCacheProvider extends CacheProvider {

	public EnumCacheProvider(CacheDef def) {
		super(def);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getList() {
		Class<?> clazz = def.getClassName();
		if (!clazz.isEnum()) {
			throw new CacheException(def.getClassName() + "不是枚举类");
		}
		CacheObject cache = j2cache.get("default", clazz.getName());
		if (cache.getValue() != null) {
			return (List<T>) cache.getValue();
		}
		List<CacheDef> list = new ArrayList<>();
		for (Object obj : clazz.getEnumConstants()) {
			CacheDef cacheDef = new CacheDef();
			cacheDef.setNameField((String) Reflections.getFieldValue(obj, def.getNameField()));
			cacheDef.setValueField((String) Reflections.getFieldValue(obj, def.getValueField()));
			list.add(cacheDef);
		}
		j2cache.set("default", clazz.getName(), list);
		return (List<T>) list;
	}

}
