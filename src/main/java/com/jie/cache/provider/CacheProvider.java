package com.jie.cache.provider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jie.cache.support.CacheDef;

import net.oschina.j2cache.CacheChannel;
import net.oschina.j2cache.J2Cache;

public abstract class CacheProvider {
	protected CacheDef def;
	protected CacheChannel j2cache = J2Cache.getChannel();

	public CacheProvider(CacheDef def) {
		this.def = def;
	}

	public abstract <T> List<T> getList();
	
	public <T> Map<String, T> getMap() {
		List<T> list = getList();
		Map<String, T> resultMap = new HashMap<>();
		for (T t : list) {
			CacheDef d = (CacheDef) t;
			resultMap.put(d.getValueField(), t);
		}
		return resultMap;
	}
}
