package com.jie.cache.support;

import java.io.Serializable;

import com.jie.cache.provider.CacheProvider;

public class CacheDef implements Serializable {
	private static final long serialVersionUID = 9166969212045101821L;

	/**
	 * The filtersql.
	 */
	private String filtersql;

	/**
	 * The basesql.
	 */
	private String basesql;

	/**
	 * The class name.
	 */
	private Class<?> className;

	/**
	 * The isleaf field.
	 */
	private String isleafField;

	/**
	 * The name field.
	 */
	private String nameField;

	/**
	 * The orderby.
	 */
	private String orderby;

	/**
	 * The parent field.
	 */
	private String parentField;

	/**
	 * The provider.
	 */
	private Class<? extends CacheProvider> provider;

	/**
	 * The value field.
	 */
	private String valueField;

	public String getFiltersql() {
		return filtersql;
	}

	public void setFiltersql(String filtersql) {
		this.filtersql = filtersql;
	}

	public String getBasesql() {
		return basesql;
	}

	public void setBasesql(String basesql) {
		this.basesql = basesql;
	}

	public Class<?> getClassName() {
		return className;
	}

	public void setClassName(Class<?> className) {
		this.className = className;
	}

	public String getIsleafField() {
		return isleafField;
	}

	public void setIsleafField(String isleafField) {
		this.isleafField = isleafField;
	}

	public String getNameField() {
		return nameField;
	}

	public void setNameField(String nameField) {
		this.nameField = nameField;
	}

	public String getOrderby() {
		return orderby;
	}

	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}

	public String getParentField() {
		return parentField;
	}

	public void setParentField(String parentField) {
		this.parentField = parentField;
	}

	public Class<? extends CacheProvider> getProvider() {
		return provider;
	}

	public void setProvider(Class<? extends CacheProvider> provider) {
		this.provider = provider;
	}

	public String getValueField() {
		return valueField;
	}

	public void setValueField(String valueField) {
		this.valueField = valueField;
	}

	@Override
	public String toString() {
		return "CacheDef [filtersql=" + filtersql + ", basesql=" + basesql + ", className=" + className
				+ ", isleafField=" + isleafField + ", nameField=" + nameField + ", orderby=" + orderby
				+ ", parentField=" + parentField + ", provider=" + provider + ", valueField=" + valueField + "]";
	}
}
