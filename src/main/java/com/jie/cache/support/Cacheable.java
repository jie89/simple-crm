package com.jie.cache.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jie.cache.provider.CacheProvider;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Cacheable {
	/**
	 * The filtersql.
	 */
	String filtersql() default "";

	/**
	 * The basesql.
	 */
	String basesql() default "";

	/**
	 * The name field.
	 */
	String nameField() default "";

	/**
	 * The orderby.
	 */
	String orderby() default "";

	/**
	 * The parent field.
	 */
	String parentField() default "";

	/**
	 * The provider.
	 */
	Class<? extends CacheProvider> provider();

	/**
	 * The value field.
	 */
	String valueField() default "";
}
