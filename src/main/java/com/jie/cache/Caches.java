package com.jie.cache;

import java.util.List;
import java.util.Map;

import net.oschina.j2cache.CacheException;

import org.springframework.core.annotation.AnnotationUtils;

import com.jie.cache.provider.CacheProvider;
import com.jie.cache.support.CacheDef;
import com.jie.cache.support.Cacheable;

public class Caches {

	private Caches() {
	}

	public static <T> List<T> getList(Class<?> clazz) {
		Cacheable annotation = AnnotationUtils.findAnnotation(clazz, Cacheable.class);
		CacheDef def = new CacheDef();
		def.setClassName(clazz);
		def.setBasesql(annotation.basesql());
		def.setFiltersql(annotation.filtersql());
		def.setNameField(annotation.nameField());
		def.setOrderby(annotation.orderby());
		def.setParentField(annotation.parentField());
		def.setProvider(annotation.provider());
		def.setValueField(annotation.valueField());
		Class<? extends CacheProvider> provider = def.getProvider();
		try {
			return provider.getConstructor(CacheDef.class).newInstance(def).getList();
		} catch (Exception e) {
			throw new CacheException("获取缓存失败", e);
		}
	}

	public static <T> Map<String, T> getMap(Class<?> clazz) {
		CacheDef def = parseCacheDef(clazz);
		Class<? extends CacheProvider> provider = def.getProvider();
		try {
			return provider.getConstructor(CacheDef.class).newInstance(def).getMap();
		} catch (Exception e) {
			throw new CacheException("获取缓存失败", e);
		}
	}

	public static <T> T get(Class<?> clazz, String key) {
		CacheDef def = parseCacheDef(clazz);
		Class<? extends CacheProvider> provider = def.getProvider();
		try {
			Map<String, T> map = provider.getConstructor(CacheDef.class).newInstance(def).getMap();
			return map.get(key);
		} catch (Exception e) {
			throw new CacheException("获取缓存失败", e);
		}
	}

	private static <T> CacheDef parseCacheDef(Class<T> clazz) {
		Cacheable annotation = AnnotationUtils.findAnnotation(clazz, Cacheable.class);
		CacheDef def = new CacheDef();
		def.setClassName(clazz);
		def.setBasesql(annotation.basesql());
		def.setFiltersql(annotation.filtersql());
		def.setNameField(annotation.nameField());
		def.setOrderby(annotation.orderby());
		def.setParentField(annotation.parentField());
		def.setProvider(annotation.provider());
		def.setValueField(annotation.valueField());
		return def;
	}
}
