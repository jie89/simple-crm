package com.jie.crm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.data.jpa.tools.DBHelper;

import com.jie.crm.domain.CustomerSource;
import com.jie.crm.repository.CustomerSourceRepository;

/**
 * 
 * <br>
 * 时间： 2018年2月27日 下午10:12:19
 * @author wangjie
 *
 */
@Service
public class CustomerSourceService {
    @Autowired
    private CustomerSourceRepository customerSourceRepository;

    public List<CustomerSource> findAll() {
        return DBHelper.bind(customerSourceRepository).findAll();
    }

}
