package com.jie.crm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jie.crm.domain.CustomerCondition;
import com.jie.crm.repository.CustomerConditionRepository;

/**
 * 
 * <br>
 * 时间： 2018年2月27日 下午10:12:19
 * @author wangjie
 *
 */
@Service
public class CustomerConditionService {
    @Autowired
    private CustomerConditionRepository customerConditionRepository;

    public List<CustomerCondition> findAll() {
        return customerConditionRepository.findAll();
    }

}
