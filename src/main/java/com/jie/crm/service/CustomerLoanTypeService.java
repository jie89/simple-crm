package com.jie.crm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.CommonJpaRepository;
import org.springframework.stereotype.Service;

import spring.data.jpa.tools.DBHelper;

import com.jie.crm.domain.CustomerLoanType;

/**
 * 
 * <br>
 * 时间： 2018年2月27日 下午10:12:19
 * @author wangjie
 *
 */
@Service
public class CustomerLoanTypeService {
    @Autowired
    @Qualifier("customerLoanTypeRepository")
    private CommonJpaRepository<CustomerLoanType, Integer> customerLoanTypeRepository;

    public List<CustomerLoanType> findAll() {
        return DBHelper.bind(customerLoanTypeRepository).findAll();
    }

}
