package com.jie.crm.utils;

import org.springframework.ui.Model;

/**
 * 
 * <br>
 * 时间： 2018年3月5日 下午10:39:22
 * @author wangjie
 *
 */
public class ModelHelper {

    private ModelHelper() {
    }

    public static void add(Model model, String name, Object value) {
        if (value != null) {
            model.addAttribute(name, value);
        }
    }
}
