package com.jie.crm.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import spring.data.jpa.tools.SpringContextHolder;

/**
 * 
 * <br>
 * 时间： 2018年2月27日 下午9:50:10
 * @author wangjie
 *
 */
@Configuration
public class DataConfig {

    @Bean(name = "dataSource")
    @Qualifier(value = "dataSource")
    @Primary
    @ConfigurationProperties(prefix = "c3p0")
    public DataSource dataSource() {
        return DataSourceBuilder.create().type(com.mchange.v2.c3p0.ComboPooledDataSource.class).build();
    }
    
    @Bean
    public SpringContextHolder getContext(){
    	return new SpringContextHolder();
    }
}
