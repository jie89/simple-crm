package com.jie.crm.config;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Hello world!
 *
 */
@DisallowConcurrentExecution
public class AppJobDetail extends QuartzJobBean {

    private Logger logger = LoggerFactory.getLogger(AppJobDetail.class);

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        logger.info("AppJobDetail running:{}", context.getJobDetail().getJobDataMap().get("Job"));
    }

}
