package com.jie.crm.config;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * <p>
 * 标题: 
 * </p>
	* <p>
 * 描述: 
 * </p>
 * <p>
 * 版权: 税友软件集团股份有限公司
 * </p>
 * <p>
	* 创建时间: 2017年12月5日
 * </p>  
 * <p>
	* 作者: wangjie
 * </p>
 * <p>修改历史记录：</p>
 * ====================================================================<br>
 * 维护单：<br>
 * 修改日期：<br>
 * 修改人：<br>
 * 修改内容：<br>      
 */
@Configuration
public class JobDetailConfig {

    private static JobDetailFactoryBean createJobDetail(Class<? extends Job> jobClass, String description) {
        JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
        factoryBean.setJobClass(jobClass);
        factoryBean.setDurability(true);
        factoryBean.setRequestsRecovery(true);
        factoryBean.setDescription(description);
        return factoryBean;
    }

    private static CronTriggerFactoryBean createTrigger(JobDetail jobDetail, String cronExpression) {
        CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
        factoryBean.setJobDetail(jobDetail);
        factoryBean.setCronExpression(cronExpression);
        return factoryBean;
    }

    @Bean
    public CronTriggerFactoryBean hnLhjyCbdjJsTrigger(@Qualifier("hnLhjyCbdjJsJobDetail") JobDetail jobDetail) {
        return createTrigger(jobDetail, "0/20 * * * * ?");
    }

    @Bean
    public JobDetailFactoryBean hnLhjyCbdjJsJobDetail() {
        JobDetailFactoryBean factory = createJobDetail(AppJobDetail.class, "测试");
        factory.getJobDataMap().put("Job", "hnLhjyCbdjJsJobDetail");
        return factory;
    }

    @Bean
    public CronTriggerFactoryBean hnCbxzJsTrigger(@Qualifier("hnCbxzJsJobDetail") JobDetail jobDetail) {
        return createTrigger(jobDetail, "0,30 * * * * ?");
    }

    @Bean
    public JobDetailFactoryBean hnCbxzJsJobDetail() {
        JobDetailFactoryBean factory = createJobDetail(AppJobDetail.class, "测试1");
        factory.getJobDataMap().put("Job", "hnCbxzJsJobDetail");
        return factory;
    }
}
