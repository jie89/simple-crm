package com.jie.crm.config;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * 1.实体类中加上@DateTimeFormat<br>
 * 2.Controller 中增加@InitBinder<br>
 * 3.扩展接口HandlerMethodArgumentResolver<br>
 * 4.注册Converter<br>
 * 
 * @author wangjie
 *
 */
@Component
public class DateConverter implements Converter<String, Date> {
	private final Logger logger = LoggerFactory.getLogger(DateConverter.class);

	@Override
	public Date convert(String source) {
		if (StringUtils.isBlank(source)) {
			return null;
		}
		try {
			return DateUtils.parseDate(source, "yyyy-MM-dd",
					"yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e) {
			logger.error("", e);
		}
		return null;
	}

}
