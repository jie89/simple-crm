package com.jie.crm.config;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web 请求相关，会自动由DelegatingWebMvcConfiguration注入
 * @author wangjie
 *
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {
	@Override
	public void addArgumentResolvers(
			List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(new RResolver());
	}
}
