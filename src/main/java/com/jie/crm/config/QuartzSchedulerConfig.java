package com.jie.crm.config;

import java.io.IOException;

import javax.sql.DataSource;

import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

/**
 * 标题: trunk
 * 描述:
 * 版权: 税友软件集团股份有限公司
 * 作者: caizh
 * 时间: 2017/8/11 16:26
 */
@Configuration
public class QuartzSchedulerConfig {

    @Bean
    public JobFactory jobFactory(ApplicationContext applicationContext) {
        return new SpringBeanJobFactory();
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(DataSource dataSource, JobFactory jobFactory,
            Trigger[] jobTriggers) throws IOException {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setAutoStartup(false);
        factory.setApplicationContextSchedulerContextKey("applicationContext");
//        factory.setOverwriteExistingJobs(true);
//        factory.setDataSource(dataSource);
        factory.setJobFactory(jobFactory);
//        factory.setStartupDelay(5);
//        factory.setConfigLocation(new ClassPathResource("quartz-cluster.properties"));
        factory.setTriggers(jobTriggers);

        return factory;
    }

}
