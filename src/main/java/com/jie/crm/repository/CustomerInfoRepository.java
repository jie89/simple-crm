package com.jie.crm.repository;

import org.springframework.data.jpa.repository.CommonJpaRepository;

import com.jie.crm.domain.CustomerInfo;

/**
 * 
 * <br>
 * 时间： 2018年2月27日 下午10:13:02
 * @author wangjie
 *
 */
public interface CustomerInfoRepository extends CommonJpaRepository<CustomerInfo, Integer> {

}
