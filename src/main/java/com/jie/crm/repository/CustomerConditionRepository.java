package com.jie.crm.repository;

import org.springframework.data.jpa.repository.CommonJpaRepository;

import com.jie.crm.domain.CustomerCondition;

/**
 * 
 * <br>
 * 时间： 2018年3月5日 下午10:31:08
 * 
 * @author wangjie
 *
 */
public interface CustomerConditionRepository extends CommonJpaRepository<CustomerCondition, Integer> {

}
