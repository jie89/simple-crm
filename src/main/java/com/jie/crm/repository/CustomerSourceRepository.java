package com.jie.crm.repository;

import org.springframework.data.jpa.repository.CommonJpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.jie.crm.domain.CustomerSource;

/**
 * 
 * <br>
 * 时间： 2018年3月5日 下午10:31:08
 * @author wangjie
 *
 */
public interface CustomerSourceRepository
        extends CommonJpaRepository<CustomerSource, Integer>, JpaSpecificationExecutor<CustomerSource> {

}
