package com.jie.crm.domain;

/**
 * 客户进展记录
 * @author wangjie
 *
 */
public class CustomerProcessor {

    /**
     * 客户对应的数目
     */
    private int id;

    /**
     * 客户编号
     */
    private int customerId;

    /**
     * 客户已有资料
     */
    private String customerMaterial;

    /**
     * 客户需求
     */
    private String customerDemand;

    /**
     * 公司情况、注册情况
     */
    private String company;

    /**
     * 车产情况
     */
    private String car;

    /**
     * 房产情况
     */
    private String house;

    /**
     * 按揭贷款情况
     */
    private String loan;

    /**
     * 信贷情况
     */
    private String creditLoan;

    /**
     * 信用卡
     */
    private String creditCard;

    /**
     * 贷款计划、方案
     */
    private String plan;

    /**
     * 其他资料、或补充材料
     */
    private String otherMaterial;

    /**
     * 详细信息
     */
    private String remark;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the customerDemand
     */
    public String getCustomerDemand() {
        return customerDemand;
    }

    /**
     * @param customerDemand the customerDemand to set
     */
    public void setCustomerDemand(String customerDemand) {
        this.customerDemand = customerDemand;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the car
     */
    public String getCar() {
        return car;
    }

    /**
     * @param car the car to set
     */
    public void setCar(String car) {
        this.car = car;
    }

    /**
     * @return the house
     */
    public String getHouse() {
        return house;
    }

    /**
     * @param house the house to set
     */
    public void setHouse(String house) {
        this.house = house;
    }

    /**
     * @return the loan
     */
    public String getLoan() {
        return loan;
    }

    /**
     * @param loan the loan to set
     */
    public void setLoan(String loan) {
        this.loan = loan;
    }

    /**
     * @return the creditLoan
     */
    public String getCreditLoan() {
        return creditLoan;
    }

    /**
     * @param creditLoan the creditLoan to set
     */
    public void setCreditLoan(String creditLoan) {
        this.creditLoan = creditLoan;
    }

    /**
     * @return the creditCard
     */
    public String getCreditCard() {
        return creditCard;
    }

    /**
     * @param creditCard the creditCard to set
     */
    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    /**
     * @return the plan
     */
    public String getPlan() {
        return plan;
    }

    /**
     * @param plan the plan to set
     */
    public void setPlan(String plan) {
        this.plan = plan;
    }

    /**
     * @return the customerMaterial
     */
    public String getCustomerMaterial() {
        return customerMaterial;
    }

    /**
     * @param customerMaterial the customerMaterial to set
     */
    public void setCustomerMaterial(String customerMaterial) {
        this.customerMaterial = customerMaterial;
    }

    /**
     * @return the otherMaterials
     */
    public String getOtherMaterial() {
        return otherMaterial;
    }

    /**
     * @param otherMaterials the otherMaterials to set
     */
    public void setOtherMaterial(String otherMaterial) {
        this.otherMaterial = otherMaterial;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

}
