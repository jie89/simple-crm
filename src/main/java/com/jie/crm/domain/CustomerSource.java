package com.jie.crm.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jie.cache.support.Cacheable;
import com.jie.crm.provider.ObjectCacheProvider;

@Entity
@Table(name = "customer_source")
@Cacheable(nameField = "sourceName", valueField = "sourceId", provider = ObjectCacheProvider.class)
public class CustomerSource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2551499429718860741L;

	/**
	 * 客户来源编号
	 */
    @Id
	private int sourceId;

	/**
	 * 客户来源名称
	 */
	private String sourceName;

	/**
	 * 客户来源是否有效
	 */
	@Column(name="is_used")
	private String sourceIsUsed;



	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceIsUsed() {
		return sourceIsUsed;
	}

	public void setSourceIsUsed(String sourceIsUsed) {
		this.sourceIsUsed = sourceIsUsed;
	}

}
