package com.jie.crm.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jie.cache.support.Cacheable;
import com.jie.crm.provider.ObjectCacheProvider;

@Entity
@Table(name = "customer_condition")
@Cacheable(nameField = "conditionName", valueField = "conditionId", provider = ObjectCacheProvider.class)
public class CustomerCondition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5330367635020523445L;

	/**
	 * 客户状态编号
	 */
    @Id
	private int conditionId;

	/**
	 * 客户状态名称
	 */
	private String conditionName;

	/**
	 * 客户状态描述
	 */
	private String conditionExplain;

	/**
	 * 客户状态是否有效
	 */
	@Column(name="is_used")
	private String conditionIsUsed;



	public int getConditionId() {
		return conditionId;
	}

	public void setConditionId(int conditionId) {
		this.conditionId = conditionId;
	}

	public String getConditionName() {
		return conditionName;
	}

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}

	public String getConditionExplain() {
		return conditionExplain;
	}

	public void setConditionExplain(String conditionExplain) {
		this.conditionExplain = conditionExplain;
	}

	public String getConditionIsUsed() {
		return conditionIsUsed;
	}

	public void setConditionIsUsed(String conditionIsUsed) {
		this.conditionIsUsed = conditionIsUsed;
	}
}
