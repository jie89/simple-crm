package com.jie.crm.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jie.cache.support.Cacheable;
import com.jie.crm.provider.ObjectCacheProvider;

/**
 * 客户类型的实体类
 * @author SHAOLIN
 *
 */
@Entity
@Table(name = "customer_type")
@Cacheable(nameField = "typeName", valueField = "typeId", provider = ObjectCacheProvider.class)
public class CustomerType implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 925899526207289818L;

	/**
     * 客户类型编号
     */
    @Id
    private int typeId;

    /**
     * 客户类型名称
     */
    private String typeName;

    /**
     * 客户类型是否有效
     */
    private String isUsed;

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

}
