package com.jie.crm.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * 客户的实体类
 * @author SHAOLIN
 *
 */
@Entity
@Table(name = "customer_info")
public class CustomerInfo implements Serializable {

    private static final long serialVersionUID = 8427416288210964165L;

    /**
     * 客户编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer customerId;

    /**
     * 客户状态编号
     */
    private Integer conditionId;

    /**
     * 客户状态
     */
    @Transient
    private String customerCondition;

    /**
     * 客户类型编号
     */
    private Integer typeId;

    /**
     * 客户类型
     */
    @Transient
    private String customerType;

    /**
     * 客户所属员工编号
     */
    private Integer userId;

    /**
     * 客户所属员工
     */
    @Transient
    private String customerForUser;

    /**
     * 客户来源编号
     */
    private Integer sourceId;

    /**
     * 客户来源
     */
    @Transient
    private String customerSource;

    /**
     * 客户姓名
     */
    private String customerName;

    /**
     * 客户性别
     */
    private String customerSex;

    /**
     * 客户手机号码
     */
    private String customerMobile;

    /**
     * 客户QQ号码
     */
    private String customerQq;

    /**
     * 客户住址
     */
    private String customerAddress;

    /**
     * 客户邮箱
     */
    private String customerEmail;

    /**
     * 客户备注
     */
    @Column(name = "customer_remark", columnDefinition = "text")
    private String customerRemark;

    /**
     * 客户职位
     */
    private String customerJob;

    /**
     * 客户微博
     */
    private String customerBlog;

    /**
     * 客户座机号码
     */
    @Column(name = "customer_tel")
    private String customerTel;

    /**
     * 客户MSN
     */
    @Column(name = "customer_idcard")
    private String customerIdCard;

    /**
     * 客户出生日期
     */
    @Column(name = "birth_day")
    @Temporal(TemporalType.TIMESTAMP)
    private Date customerBirthday;

    /**
     * 客户添加时间
     */
    @Column(name = "customer_addtime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date customerAddtime;

    /**
     * 客户添加人
     */
    @Column(name = "customer_addman")
    private String customerAddman;

    /**
     * 客户修改时间
     */
    @Column(name = "customer_changetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date customerChangetime;

    /**
     * 客户修改人
     */
    @Column(name = "customer_changeman")
    private String customerChangeman;

    /**
     * 客户所属公司
     */
    private String customerCompany;

    /**
     * 客户是否有效
     */
    private String isUsed;

    /**
     * 户籍
     */
    private String customerHousehold;

    /**
     * 微信
     */
    private String customerWechat;

    /**
     * 单位手机
     */
    private String customerCompanyMobile;

    /**
     * 单位座机
     */
    private String customerCompanyTel;

    /**
     * 紧急联系人
     */
    private String urgentMan;

    /**
     * 紧急联系人电话
     */
    private String urgentManTel;

    /**
     * 紧急联系人关系
     */
    private String urgentRelation;

    /**
     * 紧急联系人微信
     */
    private String urgentWechat;

    /**
     * 其他联系人
     */
    private String otherMan;

    /**
     * 其他联系人电话
     */
    private String otherManTel;

    /**
     * 其他联系人关系
     */
    private String otherRelation;

    /**
     * 其他联系人微信
     */
    private String otherWechat;

    /**
     * 单位地址
     */
    private String customerCompanyAddr;
    
    /**
     * 贷款方式
     */
    private Integer loanType;
    
    /**
     * 单位地址
     */
    @Transient
    private String loanTypeName;
     
    /**
     * 贷款金额
     */
    private BigDecimal loanAmount;
    
    /**
     * 期望金额
     */
    private BigDecimal expactAmount;
    
    /**
     * 处理状态
     */
    private String state;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSex() {
        return customerSex;
    }

    public void setCustomerSex(String customerSex) {
        this.customerSex = customerSex;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerQq() {
        return customerQq;
    }

    public void setCustomerQq(String customerQq) {
        this.customerQq = customerQq;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerRemark() {
        return customerRemark;
    }

    public void setCustomerRemark(String customerRemark) {
        this.customerRemark = customerRemark;
    }

    public String getCustomerJob() {
        return customerJob;
    }

    public void setCustomerJob(String customerJob) {
        this.customerJob = customerJob;
    }

    public String getCustomerBlog() {
        return customerBlog;
    }

    public void setCustomerBlog(String customerBlog) {
        this.customerBlog = customerBlog;
    }

    public String getCustomerTel() {
        return customerTel;
    }

    public void setCustomerTel(String customerTel) {
        this.customerTel = customerTel;
    }

    public Date getCustomerBirthday() {
        return customerBirthday;
    }

    public void setCustomerBirthday(Date customerBirthday) {
        this.customerBirthday = customerBirthday;
    }

    public String getCustomerCompany() {
        return customerCompany;
    }

    public void setCustomerCompany(String customerCompany) {
        this.customerCompany = customerCompany;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public String getCustomerCondition() {
        return customerCondition;
    }

    public void setCustomerCondition(String customerCondition) {
        this.customerCondition = customerCondition;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerForUser() {
        return customerForUser;
    }

    public void setCustomerForUser(String customerForUser) {
        this.customerForUser = customerForUser;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getConditionId() {
        return conditionId;
    }

    public void setConditionId(Integer conditionId) {
        this.conditionId = conditionId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public String getCustomerSource() {
        return customerSource;
    }

    public void setCustomerSource(String customerSource) {
        this.customerSource = customerSource;
    }

    /**
     * @return the customerIdCard
     */
    public String getCustomerIdCard() {
        return customerIdCard;
    }

    /**
     * @param customerIdCard the customerIdCard to set
     */
    public void setCustomerIdCard(String customerIdCard) {
        this.customerIdCard = customerIdCard;
    }

    /**
     * @return the customerHousehold
     */
    public String getCustomerHousehold() {
        return customerHousehold;
    }

    /**
     * @param customerHousehold the customerHousehold to set
     */
    public void setCustomerHousehold(String customerHousehold) {
        this.customerHousehold = customerHousehold;
    }

    /**
     * @return the customerCompanyMobile
     */
    public String getCustomerCompanyMobile() {
        return customerCompanyMobile;
    }

    /**
     * @param customerCompanyMobile the customerCompanyMobile to set
     */
    public void setCustomerCompanyMobile(String customerCompanyMobile) {
        this.customerCompanyMobile = customerCompanyMobile;
    }

    /**
     * @return the customerCompanyTel
     */
    public String getCustomerCompanyTel() {
        return customerCompanyTel;
    }

    /**
     * @param customerCompanyTel the customerCompanyTel to set
     */
    public void setCustomerCompanyTel(String customerCompanyTel) {
        this.customerCompanyTel = customerCompanyTel;
    }

    /**
     * @return the urgentMan
     */
    public String getUrgentMan() {
        return urgentMan;
    }

    /**
     * @param urgentMan the urgentMan to set
     */
    public void setUrgentMan(String urgentMan) {
        this.urgentMan = urgentMan;
    }

    /**
     * @return the urgentManTel
     */
    public String getUrgentManTel() {
        return urgentManTel;
    }

    /**
     * @param urgentManTel the urgentManTel to set
     */
    public void setUrgentManTel(String urgentManTel) {
        this.urgentManTel = urgentManTel;
    }

    /**
     * @return the urgentRelation
     */
    public String getUrgentRelation() {
        return urgentRelation;
    }

    /**
     * @param urgentRelation the urgentRelation to set
     */
    public void setUrgentRelation(String urgentRelation) {
        this.urgentRelation = urgentRelation;
    }

    /**
     * @return the otherMan
     */
    public String getOtherMan() {
        return otherMan;
    }

    /**
     * @param otherMan the otherMan to set
     */
    public void setOtherMan(String otherMan) {
        this.otherMan = otherMan;
    }

    /**
     * @return the otherManTel
     */
    public String getOtherManTel() {
        return otherManTel;
    }

    /**
     * @param otherManTel the otherManTel to set
     */
    public void setOtherManTel(String otherManTel) {
        this.otherManTel = otherManTel;
    }

    /**
     * @return the otherRelation
     */
    public String getOtherRelation() {
        return otherRelation;
    }

    /**
     * @param otherRelation the otherRelation to set
     */
    public void setOtherRelation(String otherRelation) {
        this.otherRelation = otherRelation;
    }

    /**
     * @return the customerCompanyAddr
     */
    public String getCustomerCompanyAddr() {
        return customerCompanyAddr;
    }

    /**
     * @param customerCompanyAddr the customerCompanyAddr to set
     */
    public void setCustomerCompanyAddr(String customerCompanyAddr) {
        this.customerCompanyAddr = customerCompanyAddr;
    }

    public Date getCustomerAddtime() {
        return customerAddtime;
    }

    public void setCustomerAddtime(Date customerAddtime) {
        this.customerAddtime = customerAddtime;
    }

    public String getCustomerAddman() {
        return customerAddman;
    }

    public void setCustomerAddman(String customerAddman) {
        this.customerAddman = customerAddman;
    }

    public Date getCustomerChangetime() {
        return customerChangetime;
    }

    public void setCustomerChangetime(Date customerChangetime) {
        this.customerChangetime = customerChangetime;
    }

    public String getCustomerChangeman() {
        return customerChangeman;
    }

    public void setCustomerChangeman(String customerChangeman) {
        this.customerChangeman = customerChangeman;
    }

    public String getCustomerWechat() {
        return customerWechat;
    }

    public void setCustomerWechat(String customerWechat) {
        this.customerWechat = customerWechat;
    }

    public String getUrgentWechat() {
        return urgentWechat;
    }

    public void setUrgentWechat(String urgentWechat) {
        this.urgentWechat = urgentWechat;
    }

    public String getOtherWechat() {
        return otherWechat;
    }

    public void setOtherWechat(String otherWechat) {
        this.otherWechat = otherWechat;
    }

	public Integer getLoanType() {
		return loanType;
	}

	public void setLoanType(Integer loanType) {
		this.loanType = loanType;
	}

	public String getLoanTypeName() {
		return loanTypeName;
	}

	public void setLoanTypeName(String loanTypeName) {
		this.loanTypeName = loanTypeName;
	}

	public BigDecimal getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(BigDecimal loanAmount) {
		this.loanAmount = loanAmount;
	}

	public BigDecimal getExpactAmount() {
		return expactAmount;
	}

	public void setExpactAmount(BigDecimal expactAmount) {
		this.expactAmount = expactAmount;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
