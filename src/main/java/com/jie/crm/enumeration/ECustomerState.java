package com.jie.crm.enumeration;

import com.jie.cache.provider.EnumCacheProvider;
import com.jie.cache.support.Cacheable;

@Cacheable(nameField = "text", valueField = "code", provider = EnumCacheProvider.class)
public enum ECustomerState {

	WJD("01", "未接待"), YJD("02", "已接待"), ZLZB("03", "资料准备"), ZLTJ("04", "资料提交"), ZLSHSB("05", "资料审核失败"), ZLSHCG("06",
			"资料审核成功"), YXK("07", "已下款");

	private ECustomerState(String code, String text) {
		this.code = code;
		this.text = text;
	}

	private String code;
	private String text;

	public String getCode() {
		return code;
	}

	public String getText() {
		return text;
	}

}
