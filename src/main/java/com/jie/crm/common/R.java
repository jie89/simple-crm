package com.jie.crm.common;

/**
 * 对请求返回的包装
 * <br>
 * 时间： 2018年3月12日 下午6:36:45
 * @author wangjie
 *
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class R {
	private static final String OK = "ok";
	private static final String OK_MESSAGE = "操作成功！";
	private static final String ERROR = "error";
	private static final String ERROR_MESSAGE = "系统错误！";

	public R() {
		this.data = new Data<>();
	}

	private R(String code, String message) {
		this.data = new Data<>();
		this.code = code;
		this.message = message;
	}

	private String code;
	private String message;
	private Exception exception;
	// datagrid
	private Long total;
	private List<Object> rows;
	// request
	private Data<Object> data;
	private Pagination page;

	public Data<Object> getData() {
		return data;
	}

	public Long getTotal() {
		return total;
	}

	public List<Object> getRows() {
		return rows;
	}

	public Exception getException() {
		return exception;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public R putData(String key, Object value) {
		this.getData().put(key, value);
		return this;
	}

	public R putData(Map<String, Object> map) {
		this.getData().putAll(map);
		return this;
	}

	public R addData(Object value) {
		this.getData().add(value);
		return this;
	}

	public R addData(List<Object> list) {
		this.getData().addAll(list);
		return this;
	}

	public <T> R rows(List<T> list) {
		if (this.rows == null) {
			this.rows = new ArrayList<>(list.size());
		}
		this.rows.addAll(list);
		return this;
	}

	public R total(Long total) {
		this.total = total;
		return this;
	}

	public R total(Integer total) {
		this.total = (long) total;
		return this;
	}

	public static R ok() {
		return new R(R.OK, R.OK_MESSAGE);
	}

	public static R error() {
		return new R(R.ERROR, R.ERROR_MESSAGE);
	}

	public static R error(Exception e) {
		R r = new R(R.ERROR, R.ERROR_MESSAGE);
		r.exception = e;
		return r;
	}

	public <T> R page(Page<T> page) {
		this.total(page.getTotalElements());
		this.rows(page.getContent());
		return this;
	}

	public static class Data<T> {
		private Map<String, T> map;
		private List<T> list;

		public Map<String, T> getMap() {
			if (this.map == null) {
				this.map = new HashMap<>();
			}
			return map;
		}

		public List<T> getList() {
			if (list == null) {
				list = new ArrayList<>();
			}
			return list;
		}

		public void put(String key, T value) {
			if (map == null) {
				map = new HashMap<>();
			}
			map.put(key, value);
		}

		public void putAll(Map<String, T> map) {
			if (this.map == null) {
				this.map = new HashMap<>();
			}
			this.map.putAll(map);
		}

		public void add(T value) {
			if (this.list == null) {
				this.list = new ArrayList<>();
			}
			this.list.add(value);
		}

		public void addAll(List<T> list) {
			if (this.list == null) {
				this.list = new ArrayList<>();
			}
			this.list.addAll(list);
		}
	}

	public Pagination getPage() {
		return page;
	}

	public void setPage(Pagination page) {
		this.page = page;
	}

	/**
	 * easyUI 写死的page，rows <br>
	 * 时间： 2018年3月17日 下午3:46:58
	 * 
	 * @author wangjie
	 *
	 */
	public static class Pagination {
		// pagenation
		private int page; // 第几页 datagrid 默认传第一页为1
		private int rows; // pageSize

		public Pageable getPageable() {
			return PageRequest.of(page - 1, rows);
		}

		public int getPage() {
			return page;
		}

		public void setPage(int page) {
			this.page = page;
		}

		public int getRows() {
			return rows;
		}

		public void setRows(int rows) {
			this.rows = rows;
		}
	}
}
