package com.jie.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <br>
 * 时间： 2018年2月18日 下午9:20:13
 * @author wangjie
 *
 */
@Controller
@RequestMapping(value = "/")
public class IndexController {
    
    @RequestMapping("index")
    public String index(Model model) {
        return "index";
    }
}
