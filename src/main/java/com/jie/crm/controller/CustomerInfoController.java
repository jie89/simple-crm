package com.jie.crm.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jie.cache.Caches;
import com.jie.cache.support.CacheDef;
import com.jie.crm.common.R;
import com.jie.crm.domain.CustomerCondition;
import com.jie.crm.domain.CustomerInfo;
import com.jie.crm.domain.CustomerLoanType;
import com.jie.crm.domain.CustomerSource;
import com.jie.crm.domain.CustomerType;
import com.jie.crm.enumeration.ECustomerState;
import com.jie.crm.service.CustomerInfoService;
import com.jie.crm.utils.ModelHelper;

/**
 * <br>
 * 时间： 2018年2月18日 下午9:20:13
 * 
 * @author wangjie
 *
 */
@Controller
@RequestMapping(value = "/customer")
public class CustomerInfoController {

	@Autowired
	private CustomerInfoService customerInfoService;

	@RequestMapping("list/{viewName}")
	public String list(Model model, @PathVariable(value = "viewName", required = false) String viewName) {
		List<CustomerSource> customerSources = Caches.getList(CustomerSource.class);
		ModelHelper.add(model, "customerSources", customerSources);
		return "customer-" + StringUtils.defaultIfBlank(viewName, "info");
	}

	@ResponseBody
	@RequestMapping("query")
	public R query(R r) {
		Page<CustomerInfo> customerPage = customerInfoService.findPage(r.getData().getMap(), r.getPage().getPageable());
		return R.ok().page(customerPage);
	}
	
	@ResponseBody
    @RequestMapping("birthday")
    public R birthday(R r) {
	    Map<String, Object> paramMap = new HashMap<>();
	    Date today = DateUtils.truncate(new Date(), Calendar.DATE);
	    paramMap.put("GTE_customerBirthday", today);
	    paramMap.put("LT_customerBirthday", DateUtils.addDays(today, 4));
        Page<CustomerInfo> customerPage = customerInfoService.findPage(paramMap, r.getPage().getPageable());
        return R.ok().page(customerPage);
    }

	@RequestMapping("input")
	public String add(Model model, @RequestParam(value = "id", required = false) Integer id) {
		List<CustomerSource> customerSources = Caches.getList(CustomerSource.class);
		List<CustomerType> customerTypes = Caches.getList(CustomerType.class);
		List<CustomerCondition> customerConditions = Caches.getList(CustomerCondition.class);
		List<CustomerLoanType> customerLoanTypes = Caches.getList(CustomerLoanType.class);
		List<CacheDef> customerStates = Caches.getList(ECustomerState.class);
		CustomerInfo info = null;
		if (id != null) {
			info = customerInfoService.get(id);
		}
		ModelHelper.add(model, "customerInfo", info);
		ModelHelper.add(model, "customerSources", customerSources);
		ModelHelper.add(model, "customerTypes", customerTypes);
		ModelHelper.add(model, "customerConditions", customerConditions);
		ModelHelper.add(model, "customerLoanTypes", customerLoanTypes);
		ModelHelper.add(model, "customerStates", customerStates);
		return "customer-input";
	}

	@RequestMapping("view")
	public String view(Model model, @RequestParam(value = "id", required = true) Integer id) {
		CustomerInfo info = customerInfoService.get(id);
		ModelHelper.add(model, "customerInfo", info);
		return "customer-view";
	}

	@ResponseBody
	@RequestMapping("save")
	public R save(Model model, CustomerInfo customerInfo) {
		customerInfoService.save(customerInfo);
		return R.ok();
	}

	@ResponseBody
	@RequestMapping("delete/{id}")
	public R delete(Model model, @PathVariable("id") Integer id) {
		customerInfoService.delete(id);
		return R.ok();
	}

}
