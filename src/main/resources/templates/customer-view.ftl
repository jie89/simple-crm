<div class="easyui-panel" style="width:100%;">
	<div class="panel-header">
		<div class="panel-title">查看客户信息</div>
	</div>
    <div style="padding:10px 20px 20px 20px; background:#f8f8f8;">
    <form id="customer-form" class="easyui-form" method="post" data-options="novalidate:true">
        <table cellSpacing=1 cellPadding=0 width="100%" align=center border=0 class="table table-very table-basic">
			<colgroup>
				<col width="10%" />
				<col width="15%" />
				<col width="10%" />
				<col width="15%" />
				<col width="10%" />
				<col width="15%" />
				<col width="10%" />
				<col width="15%" />
			</colgroup>
			<tr>

				<td><div align="center">客户姓名：</div></td>
				<td><#if customerInfo?exists>${customerInfo.customerName!}</#if></td>
				<td><div align="center">身份证号码：</div></td>
				<td><#if customerInfo?exists>${customerInfo.customerIdCard!}</#if></td>
				<td><div align="center">户籍地址：</div></td>
				<td><#if customerInfo??>${customerInfo.customerAddress!}</#if></td>
				<td><div align="center">现住地址：</div></td>
				<td><#if customerInfo??>${customerInfo.customerHousehold!}</#if></td>
			</tr>

			<tr>
				<td><div align="center">客户性别：</div></td>
				<td><#if customerInfo??>${customerInfo.customerSex}</#if></td>
				<td><div align="center">客户职位：</div></td>
				<td><#if customerInfo??>${customerInfo.customerJob!}</#if></td>
				<td><div align="center">客户类型：</div></td>
				<td><#if customerInfo??>${customerInfo.customerType!}</#if></td>
				<td width="13%"><div align="center">客户来源：</div></td>
				<td><#if customerInfo??>${customerInfo.customerSource!}</#if></td>
			</tr>

			<tr>
				<td><div align="center">出生日期：</div></td>
				<td><#if customerInfo??>${customerInfo.customerBirthday!}</#if>
				</td>
				<td><div align="center">客户状态：</div></td>
				<td><#if customerInfo??>${customerInfo.customerCondition!}</#if></td>
				<td><div align="center">单位名称：</div></td>
				<td><#if customerInfo??>${customerInfo.customerCompany!}</#if>
				</td>
				<td><div align="center">单位地址：</div></td>
				<td><#if customerInfo??>${customerInfo.customerCompanyAddr!}</#if>
			</tr>

			<tr>
				<td><div align="center">客户QQ：</div></td>
				<td><#if customerInfo??>${customerInfo.customerQq!}</#if></td>
				<td><div align="center">Email：</div></td>
				<td><#if customerInfo??>${customerInfo.customerEmail!}</#if></td>
				<td><div align="center">客户微博：</div></td>
				<td><#if customerInfo??>${customerInfo.customerBlog!}</#if>
				</td>
				<td><div align="center">客户微信：</div></td>
				<td><#if customerInfo??>${customerInfo.customerWechat!}</#if>
				</td>
			</tr>

			<tr>
				<td><div align="center">客户手机：</div></td>
				<td><#if customerInfo??>${customerInfo.customerMobile!}</#if>
				</td>
				<td><div align="center">客户座机：</div></td>
				<td><#if customerInfo??>${customerInfo.customerTel!}</#if></td>
				<td><div align="center">单位手机：</div></td>
				<td><#if customerInfo??>${customerInfo.customerCompanyMobile!}</#if>
				</td>
				<td><div align="center">单位座机：</div></td>
				<td><#if customerInfo??>${customerInfo.customerCompanyTel!}</#if></td>
			</tr>

			<tr>
				<td><div align="center">录入人：</div></td>
				<td><#if customerInfo??>${customerInfo.customerAddman!}</#if>
				</td>
				<td><div align="center">录入时间：</div></td>
				<td><#if customerInfo??>${customerInfo.customerAddtime!}</#if>
				</td>
				<td><div align="center">修改人：</div></td>
				<td><#if customerInfo??>${customerInfo.customerChangeman!}</#if>
				</td>
				<td><div align="center">修改时间：</div></td>
				<td><#if customerInfo??>${customerInfo.customerChangetime!}</#if></td>
			</tr>

			<tr>
				<td><div align="center">紧急联系人姓名：</div></td>
				<td><#if customerInfo??>${customerInfo.urgentMan!}</#if></td>
				<td><div align="center">紧急联系人电话：</div></td>
				<td><#if customerInfo??>${customerInfo.urgentManTel!}</#if>
				</td>
				<td><div align="center">紧急联系人关系：</div></td>
				<td><#if customerInfo??>${customerInfo.urgentRelation!}</#if>
				</td>
				<td><div align="center">紧急联系人微信：</div></td>
				<td><#if customerInfo??>${customerInfo.urgentWechat!}</#if>
				</td>
			</tr>
			<tr>
				<td><div align="center">其他联系人姓名：</div></td>
				<td><#if customerInfo??>${customerInfo.otherMan!}</#if></td>
				<td><div align="center">其他联系人电话：</div></td>
				<td><#if customerInfo??>${customerInfo.otherManTel!}</#if>
				</td>
				<td><div align="center">其他联系人关系：</div></td>
				<td><#if customerInfo??>${customerInfo.otherRelation!}</#if>
				</td>
				<td><div align="center">其他联系人微信：</div></td>
				<td><#if customerInfo??>${customerInfo.otherWechat!}</#if>
				</td>
			</tr>
			<tr>
				<td><div align="center">贷款类型：</div></td>
				<td><#if customerInfo??>${customerInfo.loanTypeName!}</#if></td>
				<td><div align="center">实际贷款金额：</div></td>
				<td><#if customerInfo??>${customerInfo.loanAmount!}</#if>
				</td>
				<td><div align="center">期望贷款金额：</div></td>
				<td><#if customerInfo??>${customerInfo.expactAmount!}</#if>
				</td>
				<td><div align="center">处理状态：</div></td>
				<td><#if customerInfo??>${customerInfo.state!}</#if>
				</td>
			</tr>
			<tr>
				<td><div align="center">其他信息、备注：</div></td>
				<td colspan="7">
					<#if customerInfo??>${customerInfo.customerRemark!}</#if>
				</td>
			</tr>
		</table>
    </form>
    <div style="text-align:center;padding:5px">
        <a href="javascript:;" class="easyui-linkbutton" onclick="goBack()">返回</a>
    </div>
    </div>
</div>

<script type="text/javascript">
   function goBack(){
   		$.insdep.control("${request.contextPath}/customer/list");
   }
</script>
