<div class="panel" style="width:100%;border:#cfcfd1 solid 1px">
	<div class="panel-header" style="
    border-left: none;
    border-right: none;
    border-top: none;">
		<div class="panel-title">请输入客户信息</div>
	</div>
    <div style="padding:10px 20px 20px 20px; background:#f8f8f8;">
    <form id="customer-form" class="easyui-form" method="post" data-options="novalidate:true">
    	<input type="hidden" name="customerId" id="customerId" value="<#if customerInfo?exists>${customerInfo.customerId!}</#if>"/>
        <table cellSpacing=1 cellPadding=0 width="100%" align=center border=0 class="table">
			<colgroup>
				<col width="10%" />
				<col width="15%" />
				<col width="10%" />
				<col width="15%" />
				<col width="10%" />
				<col width="15%" />
				<col width="10%" />
				<col width="15%" />
			</colgroup>
			<tr>

				<td><div align="center">客户姓名：</div></td>
				<td><input type="text" maxlength="50" class="easyui-textbox" style=" width: 100%" name="customerName" value="<#if customerInfo?exists>${customerInfo.customerName!}</#if>" data-options="required:true"/></td>
				<td><div align="center">身份证号码：</div></td>
				<td><input type="text" maxlength="18" style=" width: 100%" name="customerIdCard"
					class="easyui-textbox" data-options="required:true" value="<#if customerInfo?exists>${customerInfo.customerIdCard!}</#if>"/>
				</td>
				<td><div align="center">户籍地址：</div></td>
				<td><input type="text" maxlength="250" style=" width: 100%" name="customerAddress" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerAddress!}</#if>"/>
				</td>
				<td><div align="center">现住地址：</div></td>
				<td><input type="text" maxlength="250" style=" width: 100%" name="customerHousehold" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerHousehold!}</#if>"/>
				</td>
			</tr>

			<tr>
				<td><div align="center">客户性别：</div></td>
				<td><input type="radio" name="customerSex" value="男" checked="checked" />男
					<input type="radio" name="customerSex" value="女" <#if customerInfo?? && customerInfo.customerSex ='女'> checked="checked" </#if>/>女<br />
				</td>
				<td><div align="center">客户职位：</div></td>
				<td><input type="text" maxlength="20" style=" width: 100%" name="customerJob" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerJob!}</#if>"/>
				</td>
				<td><div align="center">客户类型：</div></td>
				<td><select name="typeId" style=" width: 100%" class="easyui-combobox">
						<#list customerTypes as c> 
						<option value="${c.typeId}" <#if customerInfo?? && customerInfo.typeId?? && c.typeId == customerInfo.typeId> selected </#if> >${c.typeName}</option>
						</#list>
				</select>
				</td>
				<td width="13%"><div align="center">客户来源：</div>
				</td>
				<td width="33%"><select name="sourceId" style=" width: 100%" class="easyui-combobox">
						<#list customerSources as c>
						<option value="${c.sourceId}" <#if customerInfo?? && customerInfo.sourceId?? && c.sourceId == customerInfo.sourceId> selected </#if> >${c.sourceName}</option>
						</#list>
				</select>
				</td>
			</tr>

			<tr>
				<td><div align="center">出生日期：</div></td>
				<td><input type="text" name="customerBirthday" style=" width: 100%" class= "easyui-datebox" value="<#if customerInfo??>${customerInfo.customerBirthday!}</#if>"/>
				</td>
				<td><div align="center">客户状态：</div></td>
				<td><select name="conditionId" style=" width: 100%" class="easyui-combobox">
						<#list customerConditions as c>
						<option value="${c.conditionId}" <#if customerInfo?? && customerInfo.conditionId?? && c.conditionId = customerInfo.conditionId> selected </#if>>${c.conditionName}</option>
						</#list>
				</select>
				</td>
				<td><div align="center">单位名称：</div></td>
				<td><input type="text" maxlength="250" style=" width: 100%" name="customerCompany" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerCompany!}</#if>"/>
				</td>
				<td><div align="center">单位地址：</div></td>
				<td><input type="text" maxlength="250" style=" width: 100%" name="customerCompanyAddr" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerCompanyAddr!}</#if>"/>
			</tr>

			<tr>
				<td><div align="center">客户QQ：</div></td>
				<td><input type="text" style=" width: 100%" name="customerQq" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerQq!}</#if>"/></td>
				<td><div align="center">Email：</div></td>
				<td><input style=" width: 100%" maxlength="50" type="text" name="customerEmail" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerEmail!}</#if>"/></td>
				<td><div align="center">客户微博：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="customerBlog" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerBlog!}</#if>"/>
				</td>
				<td><div align="center">客户微信：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="customerWechat" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerWechat!}</#if>"/>
				</td>
			</tr>

			<tr>
				<td><div align="center">客户手机：</div></td>
				<td><input type="text" class="easyui-textbox" style=" width: 100%" name="customerMobile" value="<#if customerInfo??>${customerInfo.customerMobile!}</#if>"/>
				</td>
				<td><div align="center">客户座机：</div></td>
				<td><input type="text" class="easyui-textbox" style=" width: 100%"
					name="customerTel" value="<#if customerInfo??>${customerInfo.customerTel!}</#if>"/></td>
				<td><div align="center">单位手机：</div></td>
				<td><input type="text" class="easyui-textbox" style=" width: 100%" name="customerCompanyMobile" value="<#if customerInfo??>${customerInfo.customerCompanyMobile!}</#if>">
				</td>
				<td><div align="center">单位座机：</div></td>
				<td><input type="text" class="easyui-textbox" style=" width: 100%"
					name="customerCompanyTel" value="<#if customerInfo??>${customerInfo.customerCompanyTel!}</#if>"></td>
			</tr>

			<tr>
				<td><div align="center">录入人：</div></td>
				<td><input type="text" maxlength="10" style=" width: 100%" name="customerAddMan" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.customerAddMan!}</#if>"/>
				</td>
				<td><div align="center">录入时间：</div></td>
				<td><input type="text" maxlength="10" style=" width: 100%" readonly="readonly"
					name="customerAddTime" class="easyui-datebox" value="<#if customerInfo??>${customerInfo.customerAddTime!}</#if>"/>
				</td>
				<td><div align="center">修改人：</div></td>
				<td><input type="text" maxlength="10" style=" width: 100%" name="customerChangeman" class="easyui-textbox"
				value="<#if customerInfo??>${customerInfo.customerChangeman!}</#if>" />
				</td>
				<td><div align="center">修改时间：</div></td>
				<td><input type="text" maxlength="10" style=" width: 100%" class="easyui-datebox"
					name="customerChangetime" value="<#if customerInfo??>${customerInfo.customerChangetime!}</#if>"/></td>
			</tr>

			<tr>
				<td><div align="center">紧急联系人姓名：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="urgentMan" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.urgentMan!}</#if>"/></td>
				<td><div align="center">紧急联系人电话：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="urgentManTel" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.urgentManTel!}</#if>"/>
				</td>
				<td><div align="center">紧急联系人关系：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="urgentRelation" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.urgentRelation!}</#if>"/>
				</td>
				<td><div align="center">紧急联系人微信：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="urgentWechat" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.urgentWechat!}</#if>"/>
				</td>
			</tr>
			<tr>
				<td><div align="center">其他联系人姓名：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="otherMan" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.otherMan!}</#if>"/></td>
				<td><div align="center">其他联系人电话：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="otherManTel" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.otherManTel!}</#if>"/>
				</td>
				<td><div align="center">其他联系人关系：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="otherRelation" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.otherRelation!}</#if>"/>
				</td>
				<td><div align="center">其他联系人微信：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="otherWechat" class="easyui-textbox" value="<#if customerInfo??>${customerInfo.otherWechat!}</#if>"/>
				</td>
			</tr>
			<tr>
				<td><div align="center">贷款类型：</div></td>
				<td>
				<select name="loanType" style=" width: 100%" class="easyui-combobox">
						<#list customerLoanTypes as c>
						<option value="${c.typeId}" <#if customerInfo?? && customerInfo.loanType?? && c.typeId = customerInfo.loanType> selected </#if>>${c.typeName}</option>
						</#list>
				</select>
				</td>
				<td><div align="center">实际贷款金额：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="loanAmount" class="easyui-textbox" value="<#if customerInfo??>${(customerInfo.loanAmount?default(0))?string('0.00')}</#if>"/>
				</td>
				<td><div align="center">期望贷款金额：</div></td>
				<td><input type="text" maxlength="50" style=" width: 100%" name="expactAmount" class="easyui-textbox" value="<#if customerInfo??>${(customerInfo.expactAmount?default(0))?string('0.00')}</#if>"/>
				</td>
				<td><div align="center">处理状态：</div></td>
				<td>
				<select name="state" style=" width: 100%" class="easyui-combobox">
						<#list customerStates as c>
						<option value="${c.valueField}" <#if customerInfo?? && customerInfo.state?? && c.valueField = customerInfo.state> selected </#if>>${c.nameField}</option>
						</#list>
				</select>
				</td>
			</tr>
			<tr>
				<td><div align="center">其他信息、备注：</div></td>
				<td colspan="7">
					<textarea name="customerRemark"
									style="width:100%;height:500px;"><#if customerInfo??>${customerInfo.customerRemark!}</#if></textarea>
				</td>
			</tr>
		</table>
    </form>
    <div style="text-align:center;padding:5px">
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">提交</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()">重置</a>
    </div>
    </div>
</div>

<script type="text/javascript">
var editor;
$(function() {
	editor = KindEditor.create('textarea[name="customerRemark"]',{
		uploadJson : '${request.contextPath}/fileUpload',  
        fileManagerJson : '${request.contextPath}/fileManager', 
        allowFileManager : true, 
        imageSizeLimit : '10MB', //批量上传图片单张最大容量
    	imageUploadLimit : 10, //批量上传图片同时上传最多个数
		afterCreate : function() {
			this.sync(); //使用sync()去同步HTML数据
		},
		afterBlur: function () { 
			this.sync(); 
		},
		readonlyMode : false
	});
});
</script>


<script type="text/javascript">
    function submitForm(){
	    $('#customer-form').form({
		    url:"${request.contextPath}/customer/save",
		    onSubmit: function(){
				return $(this).form('enableValidation').form('validate');
		    },
		    success:function(data){
		    	var resp = $.parseJSON(data);
		    	var customerId = $("#customerId").val();
		    	$.messager.alert('提示', resp.message, 'info', function(){
		    		if (resp.code === 'ok') {
		    			if (customerId) {
		    				$.insdep.control("${request.contextPath}/customer/list");
		    			} else {
		    				$('#customer-form').form('reset');
		    				editor.html('');
		    			}
		    		}
		    	});
		    }
		});
		// submit the form
		$('#customer-form').submit();
    }
	
    function clearForm(){
        $('#customer-form').form('reset');
        editor.html('');
    }
</script>
