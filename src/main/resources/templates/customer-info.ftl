<style type="text/css">
.datagrid-header-inner {width:100%}
.datagrid-htable {width:100%}
.datagrid-btable {width:100%}
</style>
<table id="customer_datagrid" title="客户信息列表" style="height: 100%;"
	data-options="
			rownumbers:true,
			singleSelect:true,
			autoRowHeight:false,
			pagination:true,
			pageSize:10,
			method:'post',
			toolbar:'#search-box'">
	<thead>
		<tr>
			<th data-options="field:'customerName'">姓名</th>
			<th data-options="field:'customerCondition'">状态</th>
			<th data-options="field:'customerSource'">来源</th>
			<th data-options="field:'customerType'">类型</th>
			<th data-options="field:'customerWechat'">微信</th>
			<th data-options="field:'customerSex', align:'center'">性别</th>
			<th
				data-options="field:'customerMobile', align:'center'">手机</th>
			<th data-options="field:'customerJob'">职位</th>
			<th data-options="field:'customerCompany'">公司</th>
			<th data-options="field:'customerBirthday', formatter:formatDatebox">生日</th>
			<th
				data-options="field:'customerId',align:'center',formatter:operate">操作</th>
		</tr>
	</thead>
</table>
<div id="search-box">
	<form id="search-form" class="easyui-form" method="post" data-options="novalidate:true">
        <table cellSpacing=1 cellPadding=0 width="100%" align=center border=0 >
			<tr>
				<td><div align="center">姓名：</div></td>
				<td><input type="text" class="easyui-textbox" style="width:100px;" name="LIKE_customerName" /></td>
				<td><div align="center">微信：</div></td>
				<td><input type="text" style="width:120px;" name="LIKE_customerWechat" class="easyui-textbox" /></td>
				<td><div align="center">手机：</div></td>
				<td><input type="text" style="width:120px;" name="EQ_customerMobile" class="easyui-textbox" /></td>
				<td><div align="center">公司：</div></td>
				<td><input type="text" style="width:200px;" name="LIKE_customerCompany" class="easyui-textbox" /></td>
				<td><div align="center">来源：</div></td>
				<td>
					<select name="EQ_sourceId" style=" width: 100%" class="easyui-combobox">
						<option value="">--请选择--</option>
						<#list customerSources as c>
						<option value="${c.sourceId}">${c.sourceName}</option>
						</#list>
					</select>
				</td>
				<td align=center><a href="javascript:;" id="search-btn" class="easyui-linkbutton" iconCls="icon-search">Search</a></td>
			</tr>
		</table>
	</form>	
</div>

<script type="text/javascript">
	$(function() {
		$('#customer_datagrid').datagrid({
			url : '${request.contextPath}/customer/query',
			onLoadSuccess : function() {
				$('.button-edit,.button-delete').linkbutton({});
				$('.button-edit').on('click', function(){    
					var a=$(this).linkbutton("options");
					$.insdep.control("${request.contextPath}/customer/input?id=" + a.value);
				});
				$('.button-delete').on('click', function(){    
					var a=$(this).linkbutton("options");
					alert("delete "+a.value);
				});
			},
			onDblClickRow : function(index, row) {
				$.insdep.control("${request.contextPath}/customer/view?id=" + row.customerId);
			}
		});
		$("#search-btn").click(function() {
			var data = $.serializeObject("search-form");
			$('#customer_datagrid').datagrid('load', {'dataMap' : JSON.stringify(data)});
		});
	});
	function operate(value, row, index) {
		return "<a href='#' data-options='value:"+value+"' class='button-delete button-danger'>删除</a> <a href='#' data-options='value:"+value+"' class='button-edit button-default'>编辑</a>";
	}
</script>
