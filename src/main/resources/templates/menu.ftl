<!--开始左侧菜单-->
<div data-options="region:'west',border:false,bodyCls:'theme-left-layout'" style="width:200px;">
    <!--正常菜单--> 
    <div class="theme-left-normal">
        <!--theme-left-switch 如果不需要缩进按钮，删除该对象即可-->    
        <div class="left-control-switch theme-left-switch"><i class="fa fa-chevron-left fa-lg"></i></div>

        <!--start class="easyui-layout"-->
        <div class="easyui-layout" data-options="border:false,fit:true"> 
            <!--start region:'north'-->
            <div data-options="region:'north',border:false" style="height:100px;">
                <!--start theme-left-user-panel-->
                <div class="theme-left-user-panel">
                    <dl>
                        <dt>
                            <img src="${request.contextPath}/themes/insdep/images/portrait86x86.png" width="43" height="43">
                        </dt>
                        <dd>
                            <b class="badge-prompt">匿名 <i class="badge color-important">10</i></b>
                            <span>examples</span>
                            <p>安全等级：<i class="text-success">高</i></p>
                        </dd>

                    </dl>
                </div>
                <!--end theme-left-user-panel-->
            </div>   
            <!--end region:'north'-->

            <!--start region:'center'-->
            <div data-options="region:'center',border:false">

                <!--start easyui-accordion--> 
                <div class="easyui-accordion" data-options="border:false,fit:true">   
                    <div title="客户信息">   
                        <ul class="easyui-datalist" data-options="border:false,fit:true">
                            <li><a style="color:#878787;" href="#" onClick="$.insdep.control('${request.contextPath}/customer/list/info')">查询客户</a></li>
                            <li><a style="color:#878787;" href="#" onClick="$.insdep.control('${request.contextPath}/customer/input')">新增客户</a></li>
                            <li><a style="color:#878787;" href="#" onClick="$.insdep.control('${request.contextPath}/customer/list/yjd')">已接待客户</a></li>
							<li><a style="color:#878787;" href="#" onClick="$.insdep.control('${request.contextPath}/customer/list/yxk')">已下款客户</a></li>
							<li><a style="color:#878787;" href="#" onClick="$.insdep.control('${request.contextPath}/customer/list/birthday')">生日慰问</a></li>
                            <li>重大信息</li>
                        </ul>  
                    </div>   
                    <div title="个人事务">   
                        <ul class="easyui-datalist" data-options="border:false,fit:true">
                            <li>内部邮件</li>
                            <li>我的日志</li>
                            <li>我的提醒</li>
                        </ul>      
                    </div>   
                    <div title="个人设置">   
                        <ul class="easyui-datalist" data-options="border:false,fit:true">
                            <li>修改密码</li>
                        </ul>      
                    </div>

                </div>
                <!--end easyui-accordion--> 

            </div>
            <!--end region:'center'-->
        </div>  
        <!--end class="easyui-layout"-->

    </div>
    <!--最小化菜单--> 
    <div class="theme-left-minimal">
        <ul class="easyui-datalist" data-options="border:false,fit:true">
            <li><i class="fa fa-home fa-2x"></i><p>主题</p></li>
            <li><i class="fa fa-book fa-2x"></i><p>组件</p></li>
            <li><i class="fa fa-pencil fa-2x"></i><p>编辑</p></li>
            <li><i class="fa fa-cog fa-2x"></i><p>设置</p></li>
            <li><a class="left-control-switch"><i class="fa fa-chevron-right fa-2x"></i><p>打开</p></a></li>
        </ul>
    </div>

</div>
<!--结束左侧菜单-->