<style type="text/css">
.datagrid-header-inner {width:100%}
.datagrid-htable {width:100%}
.datagrid-btable {width:100%}
</style>
<table id="customer_datagrid" title="已下款客户列表" style="height: 100%"
	data-options="
			rownumbers:true,
			singleSelect:true,
			autoRowHeight:false,
			pagination:true,
			pageSize:10,
			method:'post'">
	<thead>
		<tr>
			<th data-options="field:'customerName', width:'50'">姓名</th>
			<th data-options="field:'customerCondition', width:'80'">状态</th>
			<th data-options="field:'customerSource', width:'80'">来源</th>
			<th data-options="field:'customerType', width:'80'">类型</th>
			<th data-options="field:'customerWechat', width:'80'">微信</th>
			<th data-options="field:'customerSex', width:'40', align:'center'">性别</th>
			<th data-options="field:'customerMobile', width:'90', align:'center'">手机</th>
			<th data-options="field:'customerJob', width:'100'">职位</th>
			<th data-options="field:'customerCompany', width:'120'">公司</th>
			<th data-options="field:'customerBirthday', width:'80', formatter:formatDatebox">生日</th>
			<th data-options="field:'state', width:'80'">处理进度</th>
		</tr>
	</thead>
</table>

<script type="text/javascript">
	$(function() {
		var data = {'EQ_state':'07'};
		$('#customer_datagrid').datagrid({
			url : '${request.contextPath}/customer/query',
			queryParams: {
				dataMap: JSON.stringify(data)
			},
			onLoadSuccess : function() {
				
			},
			onDblClickRow : function(index, row) {
				$.insdep.control("${request.contextPath}/customer/view?id=" + row.customerId);
			}
		});
	});
</script>
