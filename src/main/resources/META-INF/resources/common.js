jQuery.extend({
	/**
	 * 将form表单序列化为JSON对象，方便Ajax请求
	 * 
	 * @param form
	 *            jQuery对象类型的form表单对象
	 * @author wangjie
	 * @returns Object
	 */
	serializeObject : function(obj) {
		var form = obj;
		if (typeof obj === 'string') {
			form = $("#" + obj);
		}
		var array = form.serializeArray();
		if (!array.length) {
			return {};
		}
		var result = {};
		for ( var i = 0; i < array.length; i++) {
			var tmp = array[i];
			result[tmp.name] = tmp.value;
		}
		return result;
	}
});

Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, // month
        "d+": this.getDate(), // day
        "h+": this.getHours(), // hour
        "m+": this.getMinutes(), // minute
        "s+": this.getSeconds(), // second
        "q+": Math.floor((this.getMonth() + 3) / 3), // quarter
        "S": this.getMilliseconds()
        // millisecond
    }
    if (/(y+)/.test(format))
        format = format.replace(RegExp.$1, (this.getFullYear() + "")
            .substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    return format;
}
function formatDatebox(value) {
    if (value == null || value == '') {
        return '';
    }
    var dt;
    if (value instanceof Date) {
        dt = value;
    } else {
        dt = new Date(value);
    }

    return dt.format("yyyy-MM-dd"); //扩展的Date的format方法(上述插件实现)
}